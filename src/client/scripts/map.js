"use strict"

const google = require("./google")
const location = require("../../config/location")

const map = new google.maps.Map(document.getElementById("map"), {
  center: {lat: location.latitude, lng: location.longitude},
  zoom: 14,
})

module.exports = map
