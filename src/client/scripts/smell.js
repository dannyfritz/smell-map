"use strict"

const Particle = require("./Particle")
const google = require("./google")
const { getWind } = require("./wind")
const location = require("../../config/location")
let lastTime = performance.now()

let particles = []

const transformToMap = (map, context, canvasLayer) =>
{
  context.setTransform(1, 0, 0, 1, 0, 0)

  const resolutionScale = global.devicePixelRatio || 1
  const scale = Math.pow(2, map.zoom) * resolutionScale
  context.scale(scale, scale)

  const mapProjection = getMapProjection(map)
  const offset = mapProjection.fromLatLngToPoint(canvasLayer.getTopLeft())
  context.translate(-offset.x, -offset.y)
}

const getMapProjection = (map) =>
{
  return map.getProjection()
}

const getWorldPoint = (map, latitude, longitude) =>
{
  const rectLatLng = new google.maps.LatLng(latitude, longitude)
  const mapProjection = getMapProjection(map)
  return mapProjection.fromLatLngToPoint(rectLatLng)
}

let lastAddedParticle = 0
const particleAddInterval = 0.01

const update = (map, context, canvasLayer) =>
{
  const currentTime = performance.now()
  const dt = (currentTime - lastTime) / 1000
  lastTime = currentTime
  transformToMap(map, context, canvasLayer)

  const canvasWidth = canvasLayer.canvas.width
  const canvasHeight = canvasLayer.canvas.height
  context.clearRect(0, 0, canvasWidth, canvasHeight)

  const worldPoint = getWorldPoint(map, location.latitude, location.longitude)

  lastAddedParticle -= dt
  if (lastAddedParticle <= 0)
  {
    particles.push(new Particle(worldPoint.x, worldPoint.y))
    lastAddedParticle = particleAddInterval
  }

  getWind
    .then((wind) =>
    {
      const squish = Math.max(Math.min(wind.mph, 10), 0.5)
      const size = Math.max(Math.min(wind.mph/500, 0.03), 0.008)
      const gradient = context.createRadialGradient(
        worldPoint.x, worldPoint.y, 0,
        worldPoint.x, worldPoint.y, size)
      gradient.addColorStop(0.005, "hsla(80, 60%, 00%, 0.8)")
      gradient.addColorStop(0.01, "hsla(80, 60%, 20%, 0.5)")
      gradient.addColorStop(0.2, "hsla(80, 60%, 20%, 0.5)")
      gradient.addColorStop(0.5, "hsla(80, 80%, 40%, 0.3)")
      gradient.addColorStop(1, "hsla(80, 80%, 40%, 0)")
      context.strokeStyle = gradient
      context.beginPath()
      context.arc(
        worldPoint.x, worldPoint.y,
        size/2,
        wind.direction/180 - Math.PI/squish/2 + Math.PI/2,
        wind.direction/180 + Math.PI/squish/2 + Math.PI/2)
      context.lineWidth = size
      context.stroke()
      particles.forEach((particle) =>
      {
        particle.update(dt)
      })
      particles = particles.filter((particle) => !particle.dead)
      particles.forEach((particle) =>
      {
        particle.render(context)
      })
    })
}

module.exports = {
  update,
}
