"use strict"

const { getWind } = require("./wind")

class Particle
{
  constructor (x, y)
  {
    this.position = {x, y}
    this.speed = 0.002
    this.size = 0.0002
    this.life = 1
    this.lifeSpeed = 0.25
  }
  update (dt)
  {
    getWind.then((wind) =>
    {
      this.life -= dt * this.lifeSpeed
      const direction = wind.direction / 180 + Math.PI / 2
      const speed = wind.mph / 1000
      const deviation = () => (Math.random() - 0.5) * speed / 5
      this.position.x += dt * speed * Math.cos(direction) + deviation()
      this.position.y += dt * speed * Math.sin(direction) + deviation()
    })
  }
  get life ()
  {
    return Math.max(this._life, 0)
  }
  set life (value)
  {
    this._life = value
  }
  get dead ()
  {
    return this.life === 0 ? true : false
  }
  render (context)
  {
    context.fillStyle = `hsla(0, 0%, 50%, ${this.life})`
    const centerOffset = this.size / 2
    context.fillRect(
      this.position.x - centerOffset,this.position.y - centerOffset,
      this.size,this.size)
  }
}

module.exports = Particle
