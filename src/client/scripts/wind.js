"use strict"

const axios = require("axios")
const url = "/weather/wind/"

module.exports = {
  getWind: axios.get(url)
    .then((current) =>
    {
      return current.data
    }),
}
