"use strict"

const Express = require("express")
const morgan = require("morgan")
const app = new Express()
const routes = {
  static: require("./routes/static"),
  weather: require("./routes/weather"),
}

app.use(morgan("dev"))

app.use(routes.static)
app.use("/weather", routes.weather)

app.use(function(req, res, next)
{
  const err = new Error("Not Found")
  err.status = 404
  next(err)
})

app.use(function(err, req, res, next)
{
  res.status(err.status || 500)
  res.json(err.message)
})

module.exports = app
