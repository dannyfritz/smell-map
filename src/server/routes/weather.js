"use strict"
const Express = require("express")
const axios = require("axios")
const moment = require("moment")
const location = require("../../config/location")
const router = Express.Router()
const api_key = process.env.OPEN_WEATHER_MAP_API_KEY

let lastResult = null
const ttl = 3600 * 1000
let lastResultDate = 0

router.get("/wind/", function (req, res, next)
{
  const url = `https://api.openweathermap.org/data/2.5/weather?lat=${location.latitude}&lon=${location.longitude}&APPID=${api_key}`
  const timeElapsed = (Date.now() - lastResultDate)
  console.info(`Last wind request was ${moment.duration(timeElapsed).humanize()} ago.`)
  if (lastResult === null || timeElapsed > ttl)
  {
    console.log(`Requesting wind data from ${url}`)
    lastResult = axios.get(url)
    lastResultDate = Date.now()
  }
  else
  {
    console.log("Using cached wind data.")
  }
  lastResult
    .then((response) => response.data)
    .then((current) =>
    {
      console.log(current)
      res.json({
        direction: current.wind.deg,
        mph: current.wind.speed,
      })
    })
    .catch(next)
})

module.exports = router
