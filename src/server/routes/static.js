"use strict"
const path = require("path")
const Express = require("express")

module.exports = Express.static(path.join(process.cwd(), "public"))
