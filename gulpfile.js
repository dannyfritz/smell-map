"use strict"

const gulp = require("gulp")

const pattern = ["gulp/**/*.js", "!gulp/**/helper-*.js"]

const base = "./src/client"
const dest = "./public"

const paths = {
  scripts: [`${base}/scripts/main.js`],
  allScripts: [`${base}/scripts/**/*.js`],
  styles: [`${base}/styles/main.css`],
  allStyles: [`${base}/styles/**/*.css`],
  pages: [`${base}/**/*.html`],
  base: `${base}`,
  dest: `${dest}`,
}

require("load-gulp-tasks")(gulp, { paths, pattern })

gulp.task("default", ["build"])
gulp.task("dev", ["watch", "build"])
