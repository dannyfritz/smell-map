"use strict"

module.exports = {
	devtool: "source-map",
	context: `${__dirname}/source`,
	output: {
		filename: "scripts/[name].js",
	},
}
