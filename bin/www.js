#!/usr/bin/env node
"use strict"
require("dotenv").config()
const app = require("../src/server/app")
const PORT = process.env.PORT || 8080

app.listen(
  PORT,
  () =>
  {
    console.log(`Server running on port ${PORT}...`)
  }
)
