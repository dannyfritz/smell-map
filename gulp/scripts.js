"use strict"

const webpack = require("webpack-stream")
const eslint = require("gulp-eslint")

module.exports = function (gulp, options)
{
  gulp.task("scripts", () =>
  {
    return gulp.src(options.paths.scripts, {base: options.paths.base})
      .pipe(webpack(require("../webpack.config.js")))
      .on("error", function ()
      {
        this.emit("end")
      })
      .pipe(gulp.dest(options.paths.dest))
  })

  gulp.task("scripts:lint", () =>
  {
    return gulp.src(options.paths.allScripts)
      .pipe(eslint())
      .pipe(eslint.format())
      .pipe(eslint.failAfterError())
  })
}
