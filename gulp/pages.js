"use strict"

module.exports = (gulp, options) =>
{
  gulp.task("pages", () =>
	{
    return gulp.src(options.paths.pages, {base: options.paths.base})
      .pipe(gulp.dest(options.paths.dest))
  })
}
