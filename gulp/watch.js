"use strict"

module.exports = (gulp, options) =>
{
  gulp.task("watch", () =>
	{
    gulp.watch(options.paths.allScripts, ["scripts:lint", "scripts"])
    gulp.watch(options.paths.allStyles, ["styles:lint", "styles"])
    gulp.watch(options.paths.pages, ["pages"])
  })
}
