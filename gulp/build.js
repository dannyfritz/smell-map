"use strict"

module.exports = (gulp) =>
{
  gulp.task("build", ["pages", "styles:lint", "styles", "scripts:lint", "scripts"])
}
