"use strict"

const sourcemaps = require("gulp-sourcemaps")
const postcss = require("gulp-postcss")
const cssNext = require("postcss-cssnext")
const cssImport = require("postcss-import")
const stylelint = require("stylelint")

module.exports = (gulp, options) =>
{
  gulp.task("styles", () =>
  {
    const processors = [
      cssImport(),
      cssNext({
        browsers: "last 1 version, > 10%",
        features: {
          customProperties: false,
        },
      }),
    ]
    return gulp.src(options.paths.styles, {base: options.paths.base})
      .pipe(sourcemaps.init())
      .pipe(postcss(processors))
      .pipe(sourcemaps.write("."))
      .pipe(gulp.dest(options.paths.dest))
  })

  gulp.task("styles:lint", () =>
  {
    const processors = [
      stylelint(),
    ]
    return gulp.src(options.paths.allStyles, {base: options.paths.base})
      .pipe(postcss(processors))
  })
}
